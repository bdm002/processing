// https://kocoafab.cc/tutorial/view/284
// serial.print는 ascii코드를 보내어주고 println 함수를 가지고 있음
// serial.write는 숫자 자체를 보내어줌. 줄바꿈 함수가 없어서 Serial.write("hello \n")로 해야함 
// 따라서 문자가 깨어지더라도 정상인 것임. 직관과 틀림 


//  # 5  아날로그 입력 송신 

void setup() {
  Serial.begin(9600); 
}



void loop() { //1초 간격으로 아날로그 입력값을 전송한다 
    int a = analogRead(A0);
    
    Serial.write(a);
    Serial.write("\n");
    delay(1000);

}



//  # 1  송신 

void setup() {
  Serial.begin(9600); //시리얼 통신 초기화
}

void loop() { //1초 간격으로 'H'와 'L'을 주기적으로 전송한다
  Serial.write('H');
  delay(1000);
  Serial.write('L');
  delay(1000);
}



//  # 2  수신

void setup() {
  Serial.begin(9600);
}

void loop() {
  char ch = Serial.read(); //계속적으로 문자를 읽어들인다
  if(ch == 'H') { //'H'를 읽을 경우 LED을 ON 시킨다
    digitalWrite(13, HIGH);
  }
  else if(ch == 'L') { //'L'을 읽을 경우 LED를 OFF 시킨다
    digitalWrite(13, LOW);
  }
}

//  # 3  원격노드에서 1~15 숫자를 전송하면 base node에서 값을 확인하는 실험. 

void setup() {
    Serial.begin(9600);
}

int i = 0;

void loop() {
    delay (1000);
    Serial.println(i);
    if(i++ > 15 )  i = 0;
}


// # 4  base node 에서 'd'를 입력하면 원격 노드의 LED가 2초간 켜지도록 한다. 
int led = 13;

void setup() {
    pinMode(led, OUTPUT);
Serial.begin(9600);
}

void loop() {

    if (Serial.available() > 0 ) {
        if (Serial.read() == 'd') {
            digitalWrite(led, HIGH);
            delay(2000);
            digitalWrite(led, LOW);
        }
    }
}

    }
}
}
}




